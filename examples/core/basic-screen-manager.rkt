#lang racket/base

(require "raylib-racket/raylib.rkt"
         "raylib-racket/colors.rkt")

(define (main)
	(define screen-width 800)
	(define screen-height 450)

	(InitWindow screen-width screen-height "raylib [core] example - basic screen manager")
	(define current-screen 'logo)
	(define frames-counter 0)
	(SetTargetFPS 60)

	(let loop ((close? #f))
		(case current-screen
			['logo (begin
				(set! frames-counter (add1 frames-counter))
				(when (> frames-counter 120) (set! current-screen 'title)))]
			['title (begin
				(when (or (IsKeyPressed KEY_ENTER) (IsGestureDetected GESTURE_TAP))
					(set! current-screen 'gameplay)))]
			['gameplay (begin
				(when (or (IsKeyPressed KEY_ENTER) (IsGestureDetected GESTURE_TAP))
					(set! current-screen 'ending)))]
			['ending (begin
				(when (or (IsKeyPressed KEY_ENTER) (IsGestureDetected GESTURE_TAP))
					(set! current-screen 'title)))])

		(BeginDrawing)
		(ClearBackground RAYWHITE)
		;; TODO (phil): finish
		(case current-screen
			['logo (begin (DrawText "LOGO SCREEN" 20 20 40 LIGHTGRAY)
						 (DrawText "Wait for 2 seconds..." 290 220 20 GRAY))]
			['title (begin (DrawRectangle 0 0 screen-width screen-height GREEN)
						  (DrawText "TITLE SCREEN" 20 20 40 DARKGREEN)
						  (DrawText "PRESS ENTER or TAP to JUMP to GAMEPLAY SCREEN"
									120 220 20 DARKGREEN))]
			['gameplay (begin (DrawRectangle 0 0 screen-width screen-height PURPLE)
							 (DrawText "GAMEPLAY SCREEN" 20 20 40 MAROON)
							 (DrawText "PRESS ENTER or TAP to JUMP to ENDING SCREEN"
									   130 220 20 MAROON))]
			['ending (begin (DrawRectangle 0 0 screen-width screen-height BLUE)
						   (DrawText "ENDING SCREEN" 20 20 40 DARKBLUE)
						   (DrawText "PRESS ENTER or TAP to RETURN to TITLE SCREEN"
									 120 220 20 DARKBLUE))])
		(EndDrawing)

		(if close?
			(CloseWindow)
			(loop (WindowShouldClose)))))
(main)
