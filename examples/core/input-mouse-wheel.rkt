#lang racket/base

(require racket/math ; racket/math used for exact-round
		"raylib-racket/raylib.rkt"
		"raylib-racket/colors.rkt")

(define (main)
	(define screen-width 800)
	(define screen-height 450)
	
	(InitWindow screen-width screen-height "raylib [core] example - input mouse wheel")

	(define box-position-y (- (/ screen-height 2.0) 40.0))
	(define scroll-speed 4.0)

	(SetTargetFPS 60)

	(let loop ((close? #f))

		(set! box-position-y (- box-position-y (* (GetMouseWheelMove) scroll-speed)))

		(BeginDrawing)
		(ClearBackground RAYWHITE)
		(DrawRectangle (exact-round (- (/ screen-width 2.0) 40.0))
						(exact-round box-position-y) 80 80 MAROON)
		(DrawText "use mouse wheel to move the cube up and down!" 10 10 20 GRAY)
		(DrawText (format "Box position Y: ~a" box-position-y) 10 40 20 LIGHTGRAY)
		(EndDrawing)
		
		(if close?
			(CloseWindow)
			(loop (WindowShouldClose)))))

(main)
