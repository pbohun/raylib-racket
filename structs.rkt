#lang racket/base

(require ffi/unsafe
		 "enums.rkt")

(provide (all-defined-out))

(define-cstruct _Vector2 ([x _float]
                         [y _float]))

(define-cstruct _Vector3 ([x _float]
                         [y _float]
						 [z _float]))

(define-cstruct _Vector4 ([x _float]
                         [y _float]
						 [z _float]
						 [w _float]))

(define-cstruct _Quaternion ([x _float]
                         	 [y _float]
						 	 [z _float]
						 	 [w _float]))

(define-cstruct _Matrix ([m0 _float] [m4 _float] [m8 _float] [m12 _float]
						 [m1 _float] [m5 _float] [m9 _float] [m13 _float]
						 [m2 _float] [m6 _float] [m10 _float] [m14 _float]
						 [m3 _float] [m7 _float] [m11 _float] [m15 _float]))

(define-cstruct _Color ([r _uint8]
                        [g _uint8]
                        [b _uint8]
                        [a _uint8]))

(define-cstruct _Rectangle ([x _float]
							[y _float]
							[width _float]
							[height _float]))

(define-cstruct _Image ([data (_cpointer _void)]
						[width _int32]
						[height _int32]
						[mipmaps _int32]
						[format _int32]))

(define-cstruct _Texture ([id _uint32]
						  [width _int32]
						  [height _int32]
						  [mipmaps _int32]
						  [format _int32]))

(define-cstruct _Texture2D ([id _uint32]
						  [width _int32]
						  [height _int32]
						  [mipmaps _int32]
						  [format _int32]))

(define-cstruct _TextureCubemap ([id _uint32]
						  [width _int32]
						  [height _int32]
						  [mipmaps _int32]
						  [format _int32]))

(define-cstruct _RenderTexture ([id _uint32]
								[texture _Texture]
								[depth _Texture]))

(define-cstruct _RenderTexture2D ([id _uint32]
								[texture _Texture]
								[depth _Texture]))

(define-cstruct _NPatchInfo ([source _Rectangle]
							[left _int32]
							[right _int32]
							[top _int32]
							[bottom _int32]
							[layout _int32]))

(define-cstruct _CharInfo ([value _int32]
						   [offsetX _int32]
						   [offsetY _int32]
						   [advanceX _int32]
						   [image _Image]))

(define-cstruct _GlyphInfo ([value _int32]
						   [offsetX _int32]
						   [offsetY _int32]
						   [advanceX _int32]
						   [image _Image]))

(define-cstruct _Font ([baseSize _int32]
					  [glyphCount _int32]
					  [glyphPadding _int32]
					  [texture _Texture2D]
					  [recs (_cpointer _Rectangle)]
					  [glyphs (_cpointer _CharInfo)]))

(define-cstruct _Camera3D ([position _Vector3]
						  [target _Vector3]
						  [up _Vector3]
						  [fovy _float]
						  [projection _int32]))

(define-cstruct _Camera ([position _Vector3]
						  [target _Vector3]
						  [up _Vector3]
						  [fovy _float]
						  [projection _int32]))

(define-cstruct _Camera2D ([offset _Vector2]
						  [target _Vector2]
						  [rotation _float]
						  [zoom _float]))

(define-cstruct _Mesh ([vertexCount _int32]
					   [triangleCount _int32]
					   [vertices (_cpointer _float)]
					   [texcoords (_cpointer _float)]
					   [normals (_cpointer _float)]
					   [tangents (_cpointer _float)]
					   [colors (_cpointer _uint8)]
					   [indices (_cpointer _uint16)]
					   [animVertices (_cpointer _float)]
					   [animNormals (_cpointer _float)]
					   [boneIds (_cpointer _uint8)]
					   [boneWeights (_cpointer _float)]
					   [vaoId _uint32]
					   [vboId (_cpointer _uint32)]))

(define-cstruct _Shader ([id _uint32]
						 [locs (_cpointer _int32)]))

(define-cstruct _MaterialMap ([texture _Texture2D]
							  [color _Color]
							  [value _float]))

(define-cstruct _Material ([shader _Shader]
						   [maps (_cpointer _MaterialMap)]
						   [params (_array _float 4)]))

(define-cstruct _Transform ([translation _Vector3]
							[rotation _Quaternion]
							[scale _Vector3]))

(define-cstruct _BoneInfo ([name (_array _byte 32)]
						   [parent _int32]))

(define-cstruct _Model ([transform _Matrix]
						[meshCount _int32]
						[materialCount _int32]
						[meshes (_cpointer _Mesh)]
						[materials (_cpointer _Material)]
						[meshMaterial (_cpointer _int32)]
						[boneCount _int32]
						[bones (_cpointer _BoneInfo)]
						[bindPose (_cpointer _Transform)]))

(define-cstruct _ModelAnimation ([boneCount _int32]
								 [frameCount _int32]
								 [bones (_cpointer _BoneInfo)]
								 [framePoses (_cpointer (_cpointer _Transform))]))

(define-cstruct _Ray ([position _Vector3]
					  [direction _Vector3]))

(define-cstruct _RayHitInfo ([hit _bool]
							   [distance _float]
							   [point _Vector3]
							   [normal _Vector3]))

(define-cstruct _RayCollision ([hit _bool]
							   [distance _float]
							   [point _Vector3]
							   [normal _Vector3]))

(define-cstruct _BoundingBox ([min _Vector3]
							  [max _Vector3]))

(define-cstruct _Wave ([frameCount _uint32]
					   [sampleRate _uint32]
					   [sampleSize _uint32]
					   [channels _uint32]
					   [data (_cpointer _void)]))

(define _rAudioBuffer-pointer (_cpointer 'rAudioBuffer))

(define-cstruct _AudioStream ([buffer _rAudioBuffer-pointer]
							  [sampleRate _uint32]
							  [sampleSize _uint32]
							  [channels _uint32]))

(define-cstruct _Sound ([stream _AudioStream]
						[frameCount _uint32]))

(define-cstruct _Music ([stream _AudioStream]
						[frameCount _uint32]
						[looping _bool]
						[ctxType _int32]
						[ctxData (_cpointer _void)]))

(define-cstruct _VrDeviceInfo ([hResolution _int32]
							   [vResolution _int32]
							   [hScreenSize _float]
							   [vScreenSize _float]
							   [vScreenCenter _float]
							   [eyeToScreenDistance _float]
							   [lensSeparationDistance _float]
							   [interpupillaryDistance _float]
							   [lensDistortionValues (_array _float 4)]
							   [chromaAbCorrection (_array _float 4)]))

(define-cstruct _VrStereoConfig ([projection (_array _Matrix 2)]
								 [viewOffset (_array _Matrix 2)]
								 [leftLensCenter (_array _float 2)]
								 [rightLensCenter (_array _float 2)]
								 [leftScreenCenter (_array _float 2)]
								 [rightScreenCenter (_array _float 2)]
								 [scale (_array _float 2)]
								 [scaleIn (_array _float 2)]))
