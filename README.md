# Raylib Racket

Racket bindings for [raylib](https://www.raylib.com), a simple and easy-to-use library to enjoy video games programming.

## Requirements
### Ubuntu (and other debian-based distributions)
`apt-get install libgl1-mesa-dev libxi-dev libxcursor-dev libxrandr-dev libxinerama-dev`

### [Racket](https://racket-lang.org)

All of the Racket code here should be portable.
If you run a different operating system, the bindings should still work, you'll just have to use a different shared library file (e.g. libraylib.dll for Windows), and make sure you have the corresponding dependencies that raylib requires.

### Raylib (v4.0)
Look at the instructions for building raylib [here](https://github.com/raysan5/raylib).

You will need to build the shared object library (e.g. libraylib.so).

Note that these bindinds were made for the v4.0 of raylib.
Earlier or later versions may have breaking changes.

## Setup
- Place a copy of the shared library you built for raylib in the directory you want to build your application.
- Copy the raylib-racket folder into the source directory of your project.
- After writing your code you can build it with: `raco exe <main-file-name.rkt>`

Your directory structure should look like the following:
```
project-directory/
    |
    -- raylib-racket/
    |
    -- libraylib.so
    |
    -- <your files>
```

## Usage
Note that all the function and constant names are identical to the ones used in the original raylib C library. You can see all the possible functions and constants available to you by looking at the [cheatsheet](https://www.raylib.com/cheatsheet/cheatsheet.html).

## Example
This example creates a basic window. You can find a few more examples [here](examples).
I have only ported a few examples over from the C versions. If you're familiar with Racket, you should be able to look at the [C versions](https://github.com/raysan5/raylib/tree/master/examples) and take queues from those.
```
#lang racket/base

(require "raylib-racket/raylib.rkt"
         "raylib-racket/colors.rkt")

(define (main)
    (define screen-width 800)
    (define screen-height 450)

    (InitWindow screen-width screen-height "Basic Window")

    (SetTargetFPS 60)

    (let loop ((close? #f))

        (BeginDrawing)
        (ClearBackground (make-Color 255 255 255 255))
        (DrawText "Congrats! You created your first window!" 190 200 20 LIGHTGRAY)
        (EndDrawing)

        (if close?
            (CloseWindow)
            (loop (WindowShouldClose)))))

(main)
```
## Notes
I am not a Racket expert. This is my first non-trivial Racket project. It's quite possible that I missed doing things in a way that are idiomatic to Racket.

## License
raylib-racket is distributed under the BSD license. You can see it [here](LICENSE).
